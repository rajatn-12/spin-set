$(document).ready(function () {

var mode = "";
var morningBtn = document?.querySelector("#morning");
var afternoonBtn = document?.querySelector("#afternoon");
var nightBtn = document?.querySelector("#night");

afternoonBtn.disabled = true;
nightBtn.disabled = true;
// const frameCount = 50;
function frameCount(){
    return 100;
};

function numberOfFrames(){
    if(mode === "morning"){
        return 50;
    } else if(mode === "afternoon"){
        return 75;
    } else if(mode === "night"){
        return 100;
    } else {
        return null
    }  
}

function initialFrame(){
    if(mode === "morning"){
        return 0;
    } else if(mode === "afternoon"){
        return 51;
    } else if(mode === "night"){
        return 76;
    } else {
        return null
    }
}
const sliderStep = 0.0000000000000001;
let scrollPos = 0;

var restartClicked = false;

// gsap.set(window, { scrollTo: 0 }); //Initializing the frame

// ========================= IMAGES =========================

const canvas = document.getElementById("animContainerID");
const context = canvas.getContext("2d");

canvas.width = 960;
canvas.height = 540;

const currentFrame = (index) => {

        if (index <= 50) {
    if (index < 10) {
        return "https://s7ap1.scene7.com/is/image/grasimpassstage/0-5000" + 0 + index + "?$grasim_param_large$";
    } else {
        return "https://s7ap1.scene7.com/is/image/grasimpassstage/0-5000" + index + "?$grasim_param_large$";
    }
} 
   else  if (index <= 75) {

        if (index < 76) {
            return "https://s7ap1.scene7.com/is/image/grasimpassstage/50-7500" + index + "?$grasim_param_large$";
        }
    }
   else  if (index <= 100) {

    if (index < 100) {
        return "https://s7ap1.scene7.com/is/image/grasimpassstage/75-10000" + index + "?$grasim_param_large$";
    } else {
        return  "https://s7ap1.scene7.com/is/image/grasimpassstage/75-1000" + index + "?$grasim_param_large$";
    }
    }
}


var images = []

var anim = {
    // frame: initialFrame(),
    // frame: initialFrame(),
    frame: 0,
    lastFrame: -1,
    // totalFrames: frameCount,
    // totalFrames: numberOfFrames(),
    totalFrames: 100,
    images: []
};


for (let i = 0; i <= frameCount(); i++) {
    const img = new Image();
    img.src = currentFrame(i);
    images.push(img);
}


 function displayDiv(){
    if(images.length !== 101){
        document.querySelector(".frames-container").style.display = "none"
    } else {

        document.querySelector(".frames-container").style.display = "block";
        context.drawImage(images[0], 0, 0);
    }
 }

 displayDiv()
 




// ========================= FUNCTION: updateStats(thisFrame, thisProgress, thisScrollPos) =========================

function updateStats(thisFrame, thisProgress, thisScrollPos) {
    $(".framecount").html('frame: ' + thisFrame);
    $(".scrollProgress").html('scrollProgress: ' + thisProgress);
    $(".scrollPosition").html('scrollPosition: ' + thisScrollPos);
}


// ========================= FUNCTION: drawSprite() =========================

function drawSprite() {
    if (anim.frame === anim.lastFrame) {
        return;
    }


    if(anim.frame <= numberOfFrames()){


        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(images[anim.frame], 0, 0);
    
        anim.lastFrame = anim.frame;
 
        const thisProgress = anim.frame / frameCount();
        scrollPos = (document.body.offsetHeight - window.innerHeight) * thisProgress;
    
        // gsap.set(window, { scrollTo: (document.body.offsetHeight - window.innerHeight) * thisProgress });
        // $("#slider").slider("value", thisProgress);
        updateStats(anim.frame, thisProgress, scrollPos);
    }
}


// ========================= TIMELINE: animTimeline =========================

var animTimeline = gsap.timeline({
    paused: true,
    onUpdate: drawSprite,
    
    scrollTrigger: {
        // markers: true,
        scrub: true,

        onLeave: () => {
            animTimeline.pause()
        },

        onLeaveBack: () => {
            if (!restartClicked) {
                animTimeline.pause()
            }
            restartClicked = false;
        }

    },
});

animTimeline
    .to(anim, {
        duration: 0.35,
        frame: 50,
        ease: "none",
        snap: "frame",
}).to(anim, {
        duration: 0.3,
        frame: 75,
        ease: "none",
        snap: "frame",
}).to(anim, {
        duration: 0.25,
        frame: 100,
        ease: "none",
        snap: "frame",
});



// ========================= CONTROLS: SCRUBBER / SLIDER =========================

// $("#slider").slider({
//     range: false,
//     min: 0,
//     max: 1,
//     step: sliderStep,

//     slide: function (event, ui) {

//         const thisProgress = ui.value;

//         gsap.set(window, { scrollTo: (document.body.offsetHeight - window.innerHeight) * thisProgress });

//         animTimeline.pause();
//     }
// });


// ========================= CONTROLS: JUMP BUTTONS =========================

// $('.jumpBtn').each(function () {

//     thisButton = $(this);

//     thisButton.click(function () {

//         const thisProgress = this.value / frameCount;

//         gsap.set(window, { scrollTo: (document.body.offsetHeight - window.innerHeight) * thisProgress });

//         animTimeline.pause();
//     });

// })



    $('#morning').click(function () { 
        if(mode === "night"){
            restartClicked = true;
            animTimeline.timeScale(0.15).restart();
        }
        mode = "morning";
        animTimeline.timeScale(0.15).play();  
        morningBtn.disabled = true;
        afternoonBtn.disabled = false;
        nightBtn.disabled = true; 
 
        
    });
    $('#afternoon').click(function () { 
        mode= "afternoon";

      
        animTimeline.timeScale(0.15).play();
        morningBtn.disabled = true;
        afternoonBtn.disabled = true;
        nightBtn.disabled = false; 
    
        
    });
    $('#night').click(function () { 
        mode= "night"
        
        animTimeline.timeScale(0.15).play();
        morningBtn.disabled = false;
        afternoonBtn.disabled = true;
        nightBtn.disabled = true;  

        
    });
    $('#pause').click(function () { animTimeline.timeScale(0.15).pause(); });
    $('#resume').click(function () { animTimeline.timeScale(0.15).resume(); });
    $('#reverse').click(function () { animTimeline.timeScale(1).reverse(); });
    $('#restart').click(function () {
        restartClicked = true;
        animTimeline.timeScale(0.15).restart();
        
    });
});

